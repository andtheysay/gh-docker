# install github cli into minimal container
FROM alpine:latest AS builder
WORKDIR /home/wd
RUN apk --no-cache add curl jq \
    && CLI_VER="$(curl -s https://api.github.com/repos/cli/cli/tags | jq -r '.[0].name' | cut -c2-)" \
    && curl -LOs "https://github.com/cli/cli/releases/download/v${CLI_VER}/gh_${CLI_VER}_linux_amd64.tar.gz" \
    && tar xzf "gh_${CLI_VER}_linux_amd64.tar.gz" "gh_${CLI_VER}_linux_amd64/bin/gh" \
    && mv "gh_${CLI_VER}_linux_amd64/bin/gh" "gh"
    
FROM alpine:latest
COPY --from=builder /home/wd/gh /usr/local/bin/gh
RUN apk add --no-cache git
CMD ["gh", "--version"]